class UserTrainingsDayAssociation < ActiveRecord::Migration
  def change
    add_reference :training_days, :user, index: true, foreign_key: true
  end
end
