class StatisticReference < ActiveRecord::Migration
  def change
    add_reference :statistic_entries, :training_day, index: true, foreign_key: true
  end
end
