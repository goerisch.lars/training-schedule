class CreateStatisticEntries < ActiveRecord::Migration
  def change
    create_table :statistic_entries do |t|
      t.string :name
      t.string :value

      t.timestamps null: false
    end
  end
end
