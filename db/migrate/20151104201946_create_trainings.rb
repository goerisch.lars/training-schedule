class CreateTrainings < ActiveRecord::Migration
  def change
    create_table :trainings do |t|
      t.string :name
      t.string :type
      t.text :note
      t.integer :length
      t.integer :sets
      t.integer :weight
      t.integer :reruns

      t.timestamps null: false
    end
  end
end
