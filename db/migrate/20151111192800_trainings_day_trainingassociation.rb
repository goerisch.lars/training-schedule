class TrainingsDayTrainingassociation < ActiveRecord::Migration
  def change
    add_reference :trainings, :training_day, index: true, foreign_key: true
  end
end
