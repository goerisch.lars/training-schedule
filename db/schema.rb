# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151130154949) do

  create_table "statistic_entries", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.string   "value",           limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "training_day_id", limit: 4
  end

  add_index "statistic_entries", ["training_day_id"], name: "index_statistic_entries_on_training_day_id", using: :btree

  create_table "training_days", force: :cascade do |t|
    t.date     "date"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "user_id",    limit: 4
  end

  add_index "training_days", ["user_id"], name: "index_training_days_on_user_id", using: :btree

  create_table "trainings", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.string   "type",            limit: 255
    t.text     "note",            limit: 65535
    t.integer  "length",          limit: 4
    t.integer  "sets",            limit: 4
    t.integer  "weight",          limit: 4
    t.integer  "reruns",          limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "training_day_id", limit: 4
  end

  add_index "trainings", ["training_day_id"], name: "index_trainings_on_training_day_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "firstName",       limit: 255
    t.string   "lastName",        limit: 255
    t.string   "emailAddress",    limit: 255
    t.string   "userName",        limit: 255
    t.date     "birthday"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "password_digest", limit: 255
  end

  add_foreign_key "statistic_entries", "training_days"
  add_foreign_key "training_days", "users"
  add_foreign_key "trainings", "training_days"
end
