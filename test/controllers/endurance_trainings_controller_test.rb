require 'test_helper'

class EnduranceTrainingsControllerTest < ActionController::TestCase
  setup do
    @endurance_training = endurance_trainings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:endurance_trainings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create endurance_training" do
    assert_difference('EnduranceTraining.count') do
      post :create, endurance_training: { length: @endurance_training.length }
    end

    assert_redirected_to endurance_training_path(assigns(:endurance_training))
  end

  test "should show endurance_training" do
    get :show, id: @endurance_training
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @endurance_training
    assert_response :success
  end

  test "should update endurance_training" do
    patch :update, id: @endurance_training, endurance_training: { length: @endurance_training.length }
    assert_redirected_to endurance_training_path(assigns(:endurance_training))
  end

  test "should destroy endurance_training" do
    assert_difference('EnduranceTraining.count', -1) do
      delete :destroy, id: @endurance_training
    end

    assert_redirected_to endurance_trainings_path
  end
end
