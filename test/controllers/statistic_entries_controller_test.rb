require 'test_helper'

class StatisticEntriesControllerTest < ActionController::TestCase
  setup do
    @statistic_entry = statistic_entries(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:statistic_entries)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create statistic_entry" do
    assert_difference('StatisticEntry.count') do
      post :create, statistic_entry: { name: @statistic_entry.name, value: @statistic_entry.value }
    end

    assert_redirected_to statistic_entry_path(assigns(:statistic_entry))
  end

  test "should show statistic_entry" do
    get :show, id: @statistic_entry
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @statistic_entry
    assert_response :success
  end

  test "should update statistic_entry" do
    patch :update, id: @statistic_entry, statistic_entry: { name: @statistic_entry.name, value: @statistic_entry.value }
    assert_redirected_to statistic_entry_path(assigns(:statistic_entry))
  end

  test "should destroy statistic_entry" do
    assert_difference('StatisticEntry.count', -1) do
      delete :destroy, id: @statistic_entry
    end

    assert_redirected_to statistic_entries_path
  end
end
