require 'test_helper'

class StrengthTrainingsControllerTest < ActionController::TestCase
  setup do
    @strength_training = strength_trainings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:strength_trainings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create strength_training" do
    assert_difference('StrengthTraining.count') do
      post :create, strength_training: { reruns: @strength_training.reruns, sets: @strength_training.sets, weight: @strength_training.weight }
    end

    assert_redirected_to strength_training_path(assigns(:strength_training))
  end

  test "should show strength_training" do
    get :show, id: @strength_training
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @strength_training
    assert_response :success
  end

  test "should update strength_training" do
    patch :update, id: @strength_training, strength_training: { reruns: @strength_training.reruns, sets: @strength_training.sets, weight: @strength_training.weight }
    assert_redirected_to strength_training_path(assigns(:strength_training))
  end

  test "should destroy strength_training" do
    assert_difference('StrengthTraining.count', -1) do
      delete :destroy, id: @strength_training
    end

    assert_redirected_to strength_trainings_path
  end
end
