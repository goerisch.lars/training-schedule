namespace :db do
  task :populate => [:environment] do

    p 'db:schema:dump'
    Rake::Task['db:schema:dump'].invoke
    p 'truncate tables'
    DatabaseCleaner.clean_with :truncation

    # User anlegen
    p 'create new entries'
    [
        {:firstName => 'Jon', :lastName => 'Schmidt', :emailAddress => 'JonnySchmidt@gmailcom', :userName => 'Jonny', :birthday => '13.06.1978', :password => '123456'},
        {:firstName => 'Anne', :lastName => 'Maler', :emailAddress => 'Anne.Maler@gmail.com', :userName => 'Anne', :birthday => '28.08.1972', :password => '123456'},
        {:firstName => 'Kathrin', :lastName => 'Reihmann', :emailAddress => 'Kati.R@gmx.de', :userName => 'Kati', :birthday => '13.03.1985', :password => '123456'},
        {:firstName => 'test', :lastName => 'test', :emailAddress => 'test@test.de', :userName => 'test', :birthday => '13.03.1985', :password => '123456'}
    ].each do |attributes|
      user = User.create(attributes)
      # F�r jeden User Trainigstage
      [
          {:date => Date.yesterday},
          {:date => Date.today},
          {:date => Date.tomorrow}
      ].each do |attributes|
        trainingsDay = TrainingDay.create(attributes)
        trainingsDay.user = user
        trainingsDay.save

        # Trainings f�r jeden Trainingstag

        [
            {:name=> 'kraft 1', :note => 'kraft note 1', :sets => '3', :weight => '50', :reruns => '12'},
            {:name=> 'kraft 2', :note => 'kraft note 2',:sets => '4', :weight => '24', :reruns => '8'},
            {:name=> 'kraft 3', :note => 'kraft note 3',:sets => '3', :weight => '30', :reruns => '10'},

        ].each do |attributes|
          strength_training = StrengthTraining.create(attributes)
          strength_training.training_day = trainingsDay;
          strength_training.save();
        end


        [
            {:name=> 'endurance 1', :note => 'endurance note 1', :length => '30'},
            {:name=> 'endurance 2', :note => 'endurance note 2', :length => '60'},

        ].each do |attributes|
          endurance_training = EnduranceTraining.create(attributes)
          endurance_training.training_day = trainingsDay
          endurance_training.save
        end

      # Statistikeintrag des Trainingstags
        [
            {:name => 'Gewicht', :value => '60 kg'},
            {:name => 'Kalorien', :value => '1800 kcal'},


        ].each do |attributes|
          statistic = StatisticEntry.create(attributes)
          statistic.training_day = trainingsDay
          statistic.save
        end

      end

    end
    p 'finish'
  end

end
