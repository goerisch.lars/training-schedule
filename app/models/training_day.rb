class TrainingDay < ActiveRecord::Base
  belongs_to :user
  has_many :trainings
  has_many :statistic_entries
end
