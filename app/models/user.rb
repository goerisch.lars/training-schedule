class User < ActiveRecord::Base
  has_one :training_day
  has_secure_password
end
