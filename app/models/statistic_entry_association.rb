class StatisticEntryAssociation < ActiveRecord::Base
  belongs_to :statistic_entry
  belongs_to :training_day
end
