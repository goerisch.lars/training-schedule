json.array!(@trainings) do |training|
  json.extract! training, :id, :name, :type, :note
  json.url training_url(training, format: :json)
end
