json.array!(@users) do |user|
  json.extract! user, :id, :firstName, :lastName, :emailAddress, :userName, :birthday
  json.url user_url(user, format: :json)
end
