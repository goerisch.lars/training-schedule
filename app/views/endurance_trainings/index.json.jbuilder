json.array!(@endurance_trainings) do |endurance_training|
  json.extract! endurance_training, :id, :length
  json.url endurance_training_url(endurance_training, format: :json)
end
