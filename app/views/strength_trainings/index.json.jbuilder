json.array!(@strength_trainings) do |strength_training|
  json.extract! strength_training, :id, :sets, :weight, :reruns
  json.url strength_training_url(strength_training, format: :json)
end
