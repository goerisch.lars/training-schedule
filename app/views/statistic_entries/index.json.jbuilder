json.array!(@statistic_entries) do |statistic_entry|
  json.extract! statistic_entry, :id, :name, :value
  json.url statistic_entry_url(statistic_entry, format: :json)
end
