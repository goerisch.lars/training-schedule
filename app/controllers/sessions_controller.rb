class SessionsController < ApplicationController
  skip_before_filter :check_session, :only=> [:new, :create]
  include SessionsHelper

  def new
    user = User.find_by(id: session[:user_id])
    if user != nil
      redirect_to weekoverview_index_path
    end
    flash.now[:info] = 'user test@test.de pw: 123456'
  end

  def create
    user = User.find_by(emailAddress: params[:session][:emailAddress].downcase)
    if user && user.authenticate(params[:session][:password])
      # Log the user in and redirect to the user's show page.
      log_in(user)
      redirect_to weekoverview_index_path
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end
end