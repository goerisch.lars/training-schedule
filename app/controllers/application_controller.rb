class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.

  protect_from_forgery with: :exception
  before_filter :check_session

  include SessionsHelper

  def check_session
    if !logged_in?
    redirect_to sessions_new_path
    end
  end
end
