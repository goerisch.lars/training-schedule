class StatisticEntriesController < ApplicationController
  before_action :set_statistic_entry, only: [:show, :edit, :update, :destroy]

  # GET /statistic_entries
  # GET /statistic_entries.json
  def index
    @statistic_entries = StatisticEntry.all
  end

  # GET /statistic_entries/1
  # GET /statistic_entries/1.json
  def show
  end

  # GET /statistic_entries/new or with /new/:date
  def new
    @date = params[:date]
    if @date != nil
      @date = Date.parse(@date)
    else
      @date = Date.today
    end
    @statistic_entry = StatisticEntry.new
    return @statistic_entry, @date
  end

  # GET /statistic_entries/1/edit
  def edit
  end

  # POST /statistic_entries
  # POST /statistic_entries.json
  def create
    @statistic_entry = StatisticEntry.new(statistic_entry_params)

    @date = params[:date]
    if @date == nil
      @date = Date.new
    else
      @date = Date.parse(@date)
    end

    trainingsDay = TrainingDay.find_by(date: @date, user: current_user)

    if trainingsDay != nil
      @statistic_entry.training_day = trainingsDay
    else
      @trainingDay = TrainingDay.new
      @trainingDay.date = @date
      @trainingDay.user = current_user
      @trainingDay.save
      @statistic_entry.training_day = @trainingDay
    end

    respond_to do |format|
      if @statistic_entry.save
        format.html { redirect_to @statistic_entry, notice: 'Statistic entry was successfully created.' }
        format.json { render :show, status: :created, location: @statistic_entry }
      else
        format.html { render :new }
        format.json { render json: @statistic_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /statistic_entries/1
  # PATCH/PUT /statistic_entries/1.json
  def update
    respond_to do |format|
      if @statistic_entry.update(statistic_entry_params)
        format.html { redirect_to @statistic_entry, notice: 'Statistic entry was successfully updated.' }
        format.json { render :show, status: :ok, location: @statistic_entry }
      else
        format.html { render :edit }
        format.json { render json: @statistic_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /statistic_entries/1
  # DELETE /statistic_entries/1.json
  def destroy
    @statistic_entry.destroy
    respond_to do |format|
      format.html { redirect_to weekoverview_index_url, notice: 'Statistic entry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_statistic_entry
      @statistic_entry = StatisticEntry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def statistic_entry_params
      params.require(:statistic_entry).permit(:name, :value)
    end
end
