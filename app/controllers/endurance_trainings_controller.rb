class EnduranceTrainingsController < ApplicationController
  before_action :set_endurance_training, only: [:show, :edit, :update, :destroy]

  # GET /endurance_trainings
  # GET /endurance_trainings.json
  def index
    @endurance_trainings = EnduranceTraining.all
  end

  # GET /endurance_trainings/1
  # GET /endurance_trainings/1.json
  def show
  end

  # GET /endurance_trainings/new
  def new
    @endurance_training = EnduranceTraining.new
  end

  # GET /endurance_trainings/1/edit
  def edit
  end

  # POST /endurance_trainings
  # POST /endurance_trainings.json
  def create
    @endurance_training = EnduranceTraining.new(endurance_training_params)

    respond_to do |format|
      if @endurance_training.save
        format.html { redirect_to @endurance_training, notice: 'Endurance training was successfully created.' }
        format.json { render :show, status: :created, location: @endurance_training }
      else
        format.html { render :new }
        format.json { render json: @endurance_training.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /endurance_trainings/1
  # PATCH/PUT /endurance_trainings/1.json
  def update
    respond_to do |format|
      if @endurance_training.update(endurance_training_params)
        format.html { redirect_to @endurance_training, notice: 'Endurance training was successfully updated.' }
        format.json { render :show, status: :ok, location: @endurance_training }
      else
        format.html { render :edit }
        format.json { render json: @endurance_training.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /endurance_trainings/1
  # DELETE /endurance_trainings/1.json
  def destroy
    @endurance_training.destroy
    respond_to do |format|
      format.html { redirect_to weekoverview_index_url, notice: 'Endurance training was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_endurance_training
      @endurance_training = EnduranceTraining.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def endurance_training_params
      params.require(:endurance_training).permit(:name, :note, :length)
    end
end
