class TrainingsController < ApplicationController
  before_action :set_training, only: [:show, :edit, :update, :destroy]

  include SessionsHelper
  # GET /trainings
  # GET /trainings.json
  def index
    @trainings = Training.all
  end

  # GET /trainings/1
  # GET /trainings/1.json
  def show
  end

  # GET /trainings/new
  def new
    @date = params[:date]
    if @date != nil
      @date = Date.parse(@date)
    else
      @date = Date.today
    end
    @training = Training.new
    return @training, @date
  end

  # GET /trainings/1/edit
  def edit
  end

  # POST /trainings
  # POST /trainings.json
  def create
    @training = Training.new(training_params)

    @date = params[:date]
    if @date == nil
      @date = Date.new
    else
      @date = Date.parse(@date)
    end

    trainingsDay = TrainingDay.find_by(date: @date, user: current_user)

    if trainingsDay != nil
      @training.training_day = trainingsDay
    else
      @trainingDay = TrainingDay.new
      @trainingDay.date = @date
      @trainingDay.user = current_user
      @trainingDay.save
      @training.training_day = @trainingDay
    end

    respond_to do |format|
      if @training.save
        format.html { redirect_to @training, notice: 'Training was successfully created.' }
        format.json { render :show, status: :created, location: @training }
      else
        format.html { render :new }
        format.json { render json: @training.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trainings/1
  # PATCH/PUT /trainings/1.json
  def update
    respond_to do |format|
      if @training.update(training_params)
        format.html { redirect_to @training, notice: 'Training was successfully updated.' }
        format.json { render :show, status: :ok, location: @training }
      else
        format.html { render :edit }
        format.json { render json: @training.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trainings/1
  # DELETE /trainings/1.json
  def destroy
    @training.destroy
    respond_to do |format|
      format.html { redirect_to weekoverview_index_url, notice: 'Training was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_training
    @training = Training.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def training_params
    params.require(:training).permit(:name, :type, :note, :sets, :length, :reruns, :weight)
  end

end
