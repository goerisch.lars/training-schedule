class WeekoverviewController < ApplicationController

  include SessionsHelper

  def index
    if request.post?
      # with posted date
      @date = params[:date]
      @array = Array.new(7)
      #@date = Date.today

      # arrayExample
      # [ day => [dayName,date] ,
      #   trainingsDay
      # ]

      p "###################" + @date
      weekArray = calculateWeekDays(@date)
      weekArray.each_with_index do |date, index|
        trainingsDay = TrainingDay.find_by(date: date, user:current_user)
        day = {
            'name' => date.strftime("%A"),
            'date' => date
        }
        @subArray = {
            'day' => day,
            'trainingsDay' => nil
        }
        if trainingsDay.present?
          @subArray['trainingsDay'] = trainingsDay
        end
        @array[index] = @subArray
      end
    else
      @array = Array.new(7)
      p "ooooooooooooooooooooooooooooooooooooooooooooo"
      # arrayExample
      # [ day => [dayName,date] ,
      #   trainingsDay
      # ]
      weekArray = calculateWeekDays(Date.today.to_s)
      weekArray.each_with_index do |date, index|
        trainingsDay = TrainingDay.find_by(date: date, user:current_user)

        day = {
            'name' => date.strftime("%A"),
            'date' => date
        }
        @subArray = {
            'day' => day,
            'trainingsDay' => nil
        }
        if trainingsDay.present?
          @subArray['trainingsDay'] = trainingsDay
        end
        @array[index] = @subArray
      end

      @date =  Date.today
      return @array, @date
    end
  end

  #
  # calculate the weekday from an given date
  # and returns them as an array
  #
  def calculateWeekDays (dateString)

    date = Date.parse(dateString)
    monday = Date.commercial(date.cwyear, date.cweek)
    return result = Array[
        monday,
        monday + 1,
        monday + 2,
        monday + 3,
        monday + 4,
        monday + 5,
        monday + 6
    ]
  end


end
