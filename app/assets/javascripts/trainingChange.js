/**
 * Created by la_goerisch on 01.12.2015.
 */

$(document).ready(function () {
    console.log("dad")

    $("#training_type").change(function () {
        console.log("dada")
        var selected = $( "#training_type option:selected" ).text()

        console.log(selected)
        if (selected === 'StrengthTraining') {
            $('.StrengthTraining').each(function (i, obj) {
                $(obj).show();
            });
            $('.EnduranceTraining').each(function (i, obj) {
                $(obj).hide();
            });
        }
        if (selected === 'EnduranceTraining') {
            $('.EnduranceTraining').each(function (i, obj) {
                $(obj).show();
            });
            $('.StrengthTraining').each(function (i, obj) {
                $(obj).hide();
            });
        }
    });
});
