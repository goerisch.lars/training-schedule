feature "Login" do

  scenario "Login with existing username and password" do
    login_with 'test@test.de', '123456'
    expect(page).to_not have_content 'Invalid email/password combination'
  end

  scenario "Login with unknown username" do
    login_with 'aaa@test.de', '123456'
    expect(page).to have_content 'Invalid email/password combination'
  end

  scenario "Login with incorrect passwort" do
    login_with 'test@test.de', '643'
    expect(page).to have_content 'Invalid email/password combination'
  end
end


def login_with(email, password)
  visit '/sessions/new'
  fill_in 'session_emailAddress', with: email
  fill_in 'session_password', with: password
  click_button 'Log in'
end

