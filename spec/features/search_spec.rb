feature "Navigate to Date" do
  scenario "Navigate to 2015-01-11" do
    login_with 'test@test.de', '123456'
    fill_in 'date', with: "2015-01-11"
    click_button 'Go'
    expect(page).to have_content('2015-01-11')
  end
end