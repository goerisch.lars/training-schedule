feature "Add statistic" do
  before :each do
    :populate
    login_with 'test@test.de', '123456'
  end

  scenario "Add statistic entry" do
    click_link 'new_stat_entry0'
    expect(page).to have_content('Statistic Entry')
    fill_in 'Name', with: "TestGewicht"
    fill_in 'Value', with: 60
    click_button 'Create Statistic entry'
    expect(page).to have_content('Statistic Entry')
    click_link 'Back'
    expect(page).to have_content('Wochenansicht')
    expect(page).to have_content("TestGewicht")
    expect(page).to have_content("60")
  end

  scenario "Remove statistic entry", :js => true do
    click_link 'new_stat_entry0'
    fill_in 'Name', with: "StatisticEntryToRemove"
    fill_in 'Value', with: 50
    click_button 'Create Statistic entry'
    click_link 'Back'
    click_link 'StatisticEntryToRemove'
    expect(page).to have_content('Statistic Entry')
    click_link 'Edit'
    expect(page).to have_content('Edit Statistic Entry')
    click_link 'Destroy'
    page.driver.browser.switch_to.alert.accept
    expect(page).to have_content('Wochenansicht')
    expect(page).to_not have_content("StatisticEntryToRemove")
  end
end
