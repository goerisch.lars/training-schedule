feature "Add training" do
  before :each do
    :populate
    login_with 'test@test.de', '123456'
  end

  scenario "Add strength training" do
    expect(page).to have_content('Wochenansicht')
    click_link 'new_training_entry0'
    expect(page).to have_content('new Training')
    fill_in 'Name', with: "Kraft-Training"
    fill_in 'Sets', with: 3
    fill_in 'Weight', with: 40
    fill_in 'Reruns', with: 8
    click_button 'Create Training'
    expect(page).to have_content('StrengthTraining')
    click_link 'Back'
    expect(page).to have_content('Wochenansicht')
    expect(page).to have_content("Kraft-Training")
  end

  scenario "Add endurance training", :js => true do
    expect(page).to have_content('Wochenansicht')
    click_link 'new_training_entry0'
    expect(page).to have_content('new Training')
    fill_in 'Name', with: "Ausdauer-Training"
    select "EnduranceTraining", :from => "training_type"
    fill_in 'Length', with: 30
    click_button 'Create Training'
    expect(page).to have_content('EnduranceTraining')
    click_link 'Back'
    expect(page).to have_content('Wochenansicht')
    expect(page).to have_content("Ausdauer-Training")
  end

  scenario "Remove strength training", :js => true do
    click_link 'new_training_entry0'
    fill_in 'Name', with: "TrainingToRemove"
    fill_in 'Sets', with: 3
    fill_in 'Weight', with: 40
    fill_in 'Reruns', with: 8
    click_button 'Create Training'
    click_link 'Back'
    click_link 'TrainingToRemove'
    expect(page).to have_content('StrengthTraining')
    click_link 'Edit'
    expect(page).to have_content('Edit StrengthTraining')
    click_link 'Destroy'
    page.driver.browser.switch_to.alert.accept
    expect(page).to have_content('Wochenansicht')
    expect(page).to_not have_content("TrainingToRemove")
  end
end


