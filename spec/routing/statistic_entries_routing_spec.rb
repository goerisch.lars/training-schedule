require "rails_helper"

RSpec.describe StatisticEntriesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/statistic_entries").to route_to("statistic_entries#index")
    end

    it "routes to #new" do
      expect(:get => "/statistic_entries/new").to route_to("statistic_entries#new")
    end

    it "routes to #show" do
      expect(:get => "/statistic_entries/1").to route_to("statistic_entries#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/statistic_entries/1/edit").to route_to("statistic_entries#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/statistic_entries").to route_to("statistic_entries#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/statistic_entries/1").to route_to("statistic_entries#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/statistic_entries/1").to route_to("statistic_entries#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/statistic_entries/1").to route_to("statistic_entries#destroy", :id => "1")
    end

  end
end
