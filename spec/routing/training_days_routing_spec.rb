require "rails_helper"

RSpec.describe TrainingDaysController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/training_days").to route_to("training_days#index")
    end

    it "routes to #new" do
      expect(:get => "/training_days/new").to route_to("training_days#new")
    end

    it "routes to #show" do
      expect(:get => "/training_days/1").to route_to("training_days#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/training_days/1/edit").to route_to("training_days#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/training_days").to route_to("training_days#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/training_days/1").to route_to("training_days#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/training_days/1").to route_to("training_days#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/training_days/1").to route_to("training_days#destroy", :id => "1")
    end

  end
end
