require "rails_helper"

RSpec.describe StrengthTrainingsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/strength_trainings").to route_to("strength_trainings#index")
    end

    it "routes to #new" do
      expect(:get => "/strength_trainings/new").to route_to("strength_trainings#new")
    end

    it "routes to #show" do
      expect(:get => "/strength_trainings/1").to route_to("strength_trainings#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/strength_trainings/1/edit").to route_to("strength_trainings#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/strength_trainings").to route_to("strength_trainings#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/strength_trainings/1").to route_to("strength_trainings#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/strength_trainings/1").to route_to("strength_trainings#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/strength_trainings/1").to route_to("strength_trainings#destroy", :id => "1")
    end

  end
end
