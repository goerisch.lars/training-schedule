require 'rails_helper'

RSpec.describe "trainings/show", type: :view do
  before(:each) do
    @training = assign(:training, Training.create!(
      :name => "Name",
      :type => "StrengthTraining",
      :note => "Note",
      :training_day => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/StrengthTraining/)
    expect(rendered).to match(/Note/)
    expect(rendered).to match(//)
  end
end
