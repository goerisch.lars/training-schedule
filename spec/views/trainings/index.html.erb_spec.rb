require 'rails_helper'

RSpec.describe "trainings/index", type: :view do
  before(:each) do
    assign(:trainings, [
      Training.create!(
        :name => "Name",
        :type => "StrengthTraining",
        :note => "Note",
        :training_day => nil
      ),
      Training.create!(
        :name => "Name",
        :type => "StrengthTraining",
        :note => "Note",
        :training_day => nil
      )
    ])
  end

  it "renders a list of trainings" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "StrengthTraining".to_s, :count => 2
    assert_select "tr>td", :text => "Note".to_s, :count => 2
  end
end
