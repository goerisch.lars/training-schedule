require 'rails_helper'

RSpec.describe "training_days/index", type: :view do
  before(:each) do
    assign(:training_days, [
      TrainingDay.create!(
        :user => nil
      ),
      TrainingDay.create!(
        :user => nil
      )
    ])
  end

  it "renders a list of training_days" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
