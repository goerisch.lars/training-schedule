require 'rails_helper'

RSpec.describe "training_days/new", type: :view do
  before(:each) do
    user = User.new(
            :firstName => "lars"
    )
    user.save
    assign(:training_day, TrainingDay.new(
      :user => user
    ))
  end

  it "renders new training_day form" do
    render

    assert_select "form[action=?][method=?]", training_days_path, "post" do

    # userid is not a valid fieldtest
    # assert_select "input#training_day_user_id[name=?]", "training_day[user_id]"
    end
  end
end
