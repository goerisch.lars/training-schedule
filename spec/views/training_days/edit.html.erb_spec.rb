require 'rails_helper'

RSpec.describe "training_days/edit", type: :view do
  before(:each) do
    user = User.new(
        :firstName => "lars"
    )
    user.save
    @training_day = assign(:training_day, TrainingDay.create!(
                                            :user => user
                                        ))
  end

  it "renders the edit training_day form" do
    render

    assert_select "form[action=?][method=?]", training_day_path(@training_day), "post" do

      # userid is not a valid fieldtest
      # assert_select "input#training_day_user_id[name=?]", "training_day[user_id]"
    end
  end
end
