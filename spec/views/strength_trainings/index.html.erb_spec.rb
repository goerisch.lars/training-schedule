require 'rails_helper'

RSpec.describe "strength_trainings/index", type: :view do
  before(:each) do
    assign(:strength_trainings, [
      StrengthTraining.create!(
        :sets => 1,
        :weight => 2,
        :reruns => 3
      ),
      StrengthTraining.create!(
        :sets => 1,
        :weight => 2,
        :reruns => 3
      )
    ])
  end

  it "renders a list of strength_trainings" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
