require 'rails_helper'

RSpec.describe "strength_trainings/edit", type: :view do
  before(:each) do
    @strength_training = assign(:strength_training, StrengthTraining.create!(
      :sets => 1,
      :weight => 1,
      :reruns => 1
    ))
  end

  it "renders the edit strength_training form" do
    render

    assert_select "form[action=?][method=?]", strength_training_path(@strength_training), "post" do

      assert_select "input#strength_training_sets[name=?]", "strength_training[sets]"

      assert_select "input#strength_training_weight[name=?]", "strength_training[weight]"

      assert_select "input#strength_training_reruns[name=?]", "strength_training[reruns]"
    end
  end
end
