require 'rails_helper'

RSpec.describe "strength_trainings/show", type: :view do
  before(:each) do
    @strength_training = assign(:strength_training, StrengthTraining.create!(
      :sets => 1,
      :weight => 2,
      :reruns => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
  end
end
