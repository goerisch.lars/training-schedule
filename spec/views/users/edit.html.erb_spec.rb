require 'rails_helper'

RSpec.describe "users/edit", type: :view do
  before(:each) do
    @user = assign(:user, User.create!(
      :firstName => "MyString",
      :lastName => "MyString",
      :emailAddress => "MyString",
      :userName => "MyString",
      :password => "123456"
    ))
  end

  it "renders the edit user form" do
    render

    assert_select "form[action=?][method=?]", user_path(@user), "post" do

      assert_select "input#user_firstName[name=?]", "user[firstName]"

      assert_select "input#user_lastName[name=?]", "user[lastName]"

      assert_select "input#user_emailAddress[name=?]", "user[emailAddress]"

      assert_select "input#user_userName[name=?]", "user[userName]"

      assert_select "input#user_userName[name=?]", "user[userName]"

      assert_select "input#user_userName[name=?]", "user[userName]"
    end
  end
end
