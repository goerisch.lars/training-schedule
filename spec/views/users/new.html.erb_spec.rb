require 'rails_helper'

RSpec.describe "users/new", type: :view do
  before(:each) do
    assign(:user, User.new(
      :firstName => "MyString",
      :lastName => "MyString",
      :emailAddress => "MyString",
      :userName => "MyString",
      :password => "123456"
    ))
  end

  it "renders new user form" do
    render

    assert_select "form[action=?][method=?]", users_path, "post" do

      assert_select "input#user_firstName[name=?]", "user[firstName]"

      assert_select "input#user_lastName[name=?]", "user[lastName]"

      assert_select "input#user_emailAddress[name=?]", "user[emailAddress]"

      assert_select "input#user_userName[name=?]", "user[userName]"
    end
  end
end
