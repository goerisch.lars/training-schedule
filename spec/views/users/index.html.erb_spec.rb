require 'rails_helper'

RSpec.describe "users/index", type: :view do
  before(:each) do
    assign(:users, [
      User.create!(
        :firstName => "First Name",
        :lastName => "Last Name",
        :emailAddress => "Email Address",
        :userName => "User Name",
        :password => "123456"
      ),
      User.create!(
        :firstName => "First Name",
        :lastName => "Last Name",
        :emailAddress => "Email Address",
        :userName => "User Name",
        :password => "123456"
      )
    ])
  end

  it "renders a list of users" do
    render
    assert_select "tr>td", :text => "First Name".to_s, :count => 2
    assert_select "tr>td", :text => "Last Name".to_s, :count => 2
    assert_select "tr>td", :text => "Email Address".to_s, :count => 2
    assert_select "tr>td", :text => "User Name".to_s, :count => 2
  end
end
