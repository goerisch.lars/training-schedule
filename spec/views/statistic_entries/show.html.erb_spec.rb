require 'rails_helper'

RSpec.describe "statistic_entries/show", type: :view do
  before(:each) do
    @statistic_entry = assign(:statistic_entry, StatisticEntry.create!(
      :name => "Name",
      :value => "Value"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Value/)
  end
end
