require 'rails_helper'

RSpec.describe "statistic_entries/edit", type: :view do
  before(:each) do
    @statistic_entry = assign(:statistic_entry, StatisticEntry.create!(
      :name => "MyString",
      :value => "MyString"
    ))
  end

  it "renders the edit statistic_entry form" do
    render

    assert_select "form[action=?][method=?]", statistic_entry_path(@statistic_entry), "post" do

      assert_select "input#statistic_entry_name[name=?]", "statistic_entry[name]"

      assert_select "input#statistic_entry_value[name=?]", "statistic_entry[value]"
    end
  end
end
