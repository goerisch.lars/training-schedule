require 'rails_helper'

RSpec.describe "statistic_entries/index", type: :view do
  before(:each) do
    assign(:statistic_entries, [
      StatisticEntry.create!(
        :name => "Name",
        :value => "Value"
      ),
      StatisticEntry.create!(
        :name => "Name",
        :value => "Value"
      )
    ])
  end

  it "renders a list of statistic_entries" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Value".to_s, :count => 2
  end
end
