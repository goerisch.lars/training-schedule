require 'rails_helper'

RSpec.describe "statistic_entries/new", type: :view do
  before(:each) do
    assign(:statistic_entry, StatisticEntry.new(
      :name => "MyString",
      :value => "MyString"
    ))
  end

  it "renders new statistic_entry form" do
    render

    assert_select "form[action=?][method=?]", statistic_entries_path, "post" do

      assert_select "input#statistic_entry_name[name=?]", "statistic_entry[name]"

      assert_select "input#statistic_entry_value[name=?]", "statistic_entry[value]"
    end
  end
end
